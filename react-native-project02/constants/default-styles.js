import { StyleSheet } from "react-native";

import Colors from "./colors";

export default StyleSheet.create({
	bodyText: {
		fontFamily: "open-sans",
	},
	titleText: {
		fontFamily: "open-sans-bold",
		fontSize: 18,
	},
	accentButton: {
		backgroundColor: Colors.accent,
	},
	primaryButton: {
		backgroundColor: Colors.primary,
	},
});
