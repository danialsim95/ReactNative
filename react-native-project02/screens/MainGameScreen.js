import React, { useState, useRef, useEffect } from "react";
import { View, Text, Alert, FlatList, StyleSheet, Dimensions } from "react-native";
import { Ionicons } from "@expo/vector-icons";

import NumberContainer from "../components/NumberContainer";
import MainButton from "../components/MainButton";
import Card from "../components/Card";
import DefaultStyles from "../constants/default-styles";

const generateRandomBetween = (min, max, exclude) => {
	min = Math.ceil(min);
	max = Math.floor(max);
	const rdmNum = Math.floor(Math.random() * (max - min)) + min;
	if (rdmNum === exclude) {
		return generateRandomBetween(min, max, exclude);
	} else {
		return rdmNum;
	}
};

const renderListItem = (listLength, itemData) => (
	<View style={styles.listItem}>
		<Text style={DefaultStyles.bodyText}>#{listLength - itemData.index}</Text>
		<Text style={DefaultStyles.bodyText}>{itemData.item}</Text>
	</View>
);

const MainGameScreen = (props) => {
	const initialGuess = generateRandomBetween(1, 100, props.userChoice);
	const [currentGuess, setCurrentGuess] = useState(initialGuess);
	const [pastGuesses, setPastGuesses] = useState([initialGuess.toString()]);
	const [availableDeviceHeight, setAvailableDeviceHeight] = useState(
		Dimensions.get("window").height
	);

	const currentLow = useRef(1);
	const currentHigh = useRef(100);

	const { userChoice, onGameOver } = props;

	useEffect(() => {
		const updateLayout = () => {
			setAvailableDeviceHeight(Dimensions.get("window").height);
		};

		Dimensions.addEventListener("change", updateLayout);
		return () => {
			Dimensions.removeEventListener("change", updateLayout);
		};
	}, []);

	useEffect(() => {
		if (currentGuess === userChoice) {
			onGameOver(pastGuesses.length);
		}
	}, [currentGuess, userChoice, onGameOver]);

	const nextGuessHandler = (direction) => {
		/* Wrong hint */
		if (
			(direction === "lower" && currentGuess < props.userChoice) ||
			(direction === "greater" && currentGuess > props.userChoice)
		) {
			Alert.alert("Don't lie!", "You know that this is wrong...", [
				{ text: "Sorry", style: "cancel" },
			]);
			return;
		}
		if (direction === "lower") {
			currentHigh.current = currentGuess;
		} else {
			currentLow.current = currentGuess + 1;
		}
		const nextNumber = generateRandomBetween(
			currentLow.current,
			currentHigh.current,
			currentGuess
		);
		setCurrentGuess(nextNumber);
		// setRounds((curRounds) => curRounds + 1);
		setPastGuesses((curPastGuesses) => [nextNumber.toString(), ...curPastGuesses]);
	};

	let gameControls = (
		<React.Fragment>
			<NumberContainer>{currentGuess}</NumberContainer>
			<Card
				style={[
					...styles.buttonContainer,
					{ marginTop: availableDeviceHeight > 600 ? 16 : 8 },
				]}
			>
				<MainButton
					onPress={nextGuessHandler.bind(this, "lower")}
					customStyle={DefaultStyles.primaryButton}
				>
					<Ionicons name="md-remove" size={24} color="white" />
				</MainButton>
				<MainButton
					onPress={nextGuessHandler.bind(this, "greater")}
					customStyle={DefaultStyles.primaryButton}
				>
					<Ionicons name="md-add" size={24} color="white" />
				</MainButton>
			</Card>
		</React.Fragment>
	);

	if (availableDeviceHeight < 500) {
		gameControls = (
			<Card style={styles.controls}>
				<MainButton
					onPress={nextGuessHandler.bind(this, "lower")}
					customStyle={DefaultStyles.primaryButton}
				>
					<Ionicons name="md-remove" size={24} color="white" />
				</MainButton>
				<NumberContainer>{currentGuess}</NumberContainer>
				<MainButton
					onPress={nextGuessHandler.bind(this, "greater")}
					customStyle={DefaultStyles.primaryButton}
				>
					<Ionicons name="md-add" size={24} color="white" />
				</MainButton>
			</Card>
		);
	}

	return (
		<View style={styles.screen}>
			<Text style={DefaultStyles.titleText}>Opponent's Guess</Text>
			{gameControls}
			<View style={styles.listContainer}>
				<FlatList
					keyExtractor={(item) => item}
					data={pastGuesses}
					renderItem={renderListItem.bind(this, pastGuesses.length)}
					contentContainerStyle={styles.list}
				/>
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	screen: {
		flex: 1,
		padding: 12,
		alignItems: "center",
	},
	buttonContainer: {
		flexDirection: "row",
		justifyContent: "space-around",
		width: 300,
		maxWidth: "80%",
	},
	controls: {
		flexDirection: "row",
		justifyContent: "space-around",
		alignItems: "center",
		width: "80%",
	},
	listContainer: {
		flex: 1,
		width: Dimensions.get("window").width > 400 ? "70%" : "80%",
		margin: 8,
	},
	list: {
		flexGrow: 1,
		// alignItems: 'center',
		justifyContent: "flex-end",
	},
	listItem: {
		borderColor: "#ccc",
		borderWidth: 1,
		padding: 16,
		marginVertical: 8,
		backgroundColor: "white",
		flexDirection: "row",
		justifyContent: "space-between",
		width: "100%",
	},
});

export default MainGameScreen;
