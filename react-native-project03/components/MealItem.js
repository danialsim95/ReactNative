import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, ImageBackground } from "react-native";

import DefaultText from "./DefaultText";

const MealItem = (props) => {
	return (
		<View style={styles.mealItem}>
			<TouchableOpacity onPress={props.onSelectMeal}>
				<View>
					<View style={{ ...styles.mealItemRow, ...styles.mealItemHeader }}>
						<ImageBackground
							style={styles.imageBackground}
							source={{ uri: props.imageUrl }}
						>
							<View style={styles.mealItemTitleContainer}>
								<Text style={styles.mealItemTitle} numberOfLines={1}>
									{props.title}
								</Text>
							</View>
						</ImageBackground>
					</View>
					<View style={{ ...styles.mealItemRow, ...styles.mealItemDetail }}>
						<DefaultText>{props.duration} min.</DefaultText>
						<DefaultText>{props.complexity.toUpperCase()}</DefaultText>
						<DefaultText>{props.affordability.toUpperCase()}</DefaultText>
					</View>
				</View>
			</TouchableOpacity>
		</View>
	);
};

const styles = StyleSheet.create({
	mealItem: {
		height: 200,
		width: "100%",
		backgroundColor: "#f5f5f5",
		borderRadius: 12,
		overflow: "hidden",
		marginVertical: 4,
		elevation: 4,
	},
	mealItemRow: {
		flexDirection: "row",
	},
	mealItemHeader: {
		height: "85%",
	},
	mealItemDetail: {
		paddingHorizontal: 12,
		justifyContent: "space-between",
		alignItems: "center",
		height: "15%",
	},
	mealItemTitle: {
		fontFamily: "open-sans-bold",
		fontSize: 18,
		color: "white",
		textAlign: "center",
	},
	mealItemTitleContainer: {
		backgroundColor: "rgba(0, 0, 0, 0.50)",
		paddingVertical: 4,
		paddingHorizontal: 12,
	},
	imageBackground: {
		width: "100%",
		height: "100%",
		justifyContent: "flex-end",
	},
});

export default MealItem;
