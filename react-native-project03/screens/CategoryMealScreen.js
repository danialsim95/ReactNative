import React from "react";
import { useSelector } from "react-redux";
import { View, StyleSheet } from "react-native";

import { CATEGORIES } from "../data/dummy-data";
import MealList from "../components/MealList";
import DefaultText from "../components/DefaultText";

const CategoryMealScreen = (props) => {
	const mealCategoryId = props.navigation.getParam("categoryId");
	const availableMeals = useSelector((state) => state.meals.filteredMeals);
	const displayedMeals = availableMeals.filter(
		(meal) => meal.categoryIds.indexOf(mealCategoryId) >= 0
	);

	if (displayedMeals.length === 0) {
		return (
			<View style={styles.fallback}>
				<DefaultText>No meals found. Please check your filters.</DefaultText>
			</View>
		);
	}

	return <MealList mealListData={displayedMeals} navigation={props.navigation} />;
};

const styles = StyleSheet.create({
	fallback: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
	},
});

CategoryMealScreen.navigationOptions = (navigationData) => {
	const mealCategoryId = navigationData.navigation.getParam("categoryId");
	const selectedCategory = CATEGORIES.find((mealCat) => mealCat.id === mealCategoryId);

	return {
		headerTitle: selectedCategory.title,
	};
};

export default CategoryMealScreen;
