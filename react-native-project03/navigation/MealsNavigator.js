import React from "react";
import { Text, Platform } from "react-native";
import { createStackNavigator } from "react-navigation-stack";
import { createBottomTabNavigator } from "react-navigation-tabs";
import { createDrawerNavigator } from "react-navigation-drawer";
import { createAppContainer } from "react-navigation";
import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs";
import { Ionicons } from "@expo/vector-icons";

import CategoryScreen from "../screens/CategoryScreen";
import CategoryMealScreen from "../screens/CategoryMealScreen";
import MealDetailScreen from "../screens/MealDetailScreen";
import FavouriteScreen from "../screens/FavouriteScreen";
import FilterScreen from "../screens/FilterScreen";

import Colors from "../constants/Colors";

const defaultStackNavOptions = {
	headerStyle: {
		backgroundColor: Platform.OS === "android" ? Colors.primaryColor : "",
	},
	headerTitleStyle: {
		fontFamily: "open-sans",
	},
	headerBackTitleStyle: {
		fontFamily: "open-sans",
	},
	headerTintColor: Platform.OS === "android" ? "white" : Colors.primaryColor,
};

const MealsNavigator = createStackNavigator(
	{
		Category: CategoryScreen,
		CategoryMeals: {
			screen: CategoryMealScreen,
		},
		MealDetail: MealDetailScreen,
	},
	{
		defaultNavigationOptions: defaultStackNavOptions,
	}
);

const FavouritesNavigator = createStackNavigator(
	{
		Favourites: FavouriteScreen,
		MealDetail: MealDetailScreen,
	},
	{
		defaultNavigationOptions: defaultStackNavOptions,
	}
);

const FilterNavigator = createStackNavigator(
	{
		Filters: FilterScreen,
	},
	{
		navigationOptions: {
			drawerLabel: "Filters",
		},
		defaultNavigationOptions: defaultStackNavOptions,
	}
);

const tabScreenConfig = {
	Meals: {
		screen: MealsNavigator,
		navigationOptions: {
			tabBarIcon: (tabInformation) => {
				return (
					<Ionicons name="ios-restaurant" size={24} color={tabInformation.tintColor} />
				);
			},
			tabBarColor: Colors.primaryColor,
			tabBarLabel:
				Platform.OS === "android" ? (
					<Text style={{ fontFamily: "open-sans-bold" }}>Meals</Text>
				) : (
					"Meals"
				),
		},
	},
	Favourites: {
		screen: FavouritesNavigator,
		navigationOptions: {
			tabBarIcon: (tabInformation) => {
				return <Ionicons name="ios-star" size={24} color={tabInformation.tintColor} />;
			},
			tabBarColor: Colors.accentColor,
			tabBarLabel:
				Platform.OS === "android" ? (
					<Text style={{ fontFamily: "open-sans-bold" }}>Favourites</Text>
				) : (
					"Favourites"
				),
		},
	},
};

const MealsFavTabNavigator =
	Platform.OS === "android"
		? createMaterialBottomTabNavigator(tabScreenConfig, {
				activeColor: "white",
				shifting: true,
				// barStyle: {
				// 	backgroundColor: Colors.primaryColor
				// }, for shifting = false use case
		  })
		: createBottomTabNavigator(tabScreenConfig, {
				tabBarOptions: {
					labelStyle: {
						fontFamily: "open-sans-bold",
					},
					activeTintColor: Colors.accentColor,
				},
		  });

const MainNavigator = createDrawerNavigator(
	{
		MealsFavourites: {
			screen: MealsFavTabNavigator,
			navigationOptions: {
				drawerLabel: "Meals",
			},
		},
		Filters: FilterNavigator,
	},
	{
		contentOptions: {
			activeTintColor: Colors.accentColor,
			labelStyle: {
				fontFamily: "open-sans",
			},
		},
	}
);

export default createAppContainer(MainNavigator);
