import moment from "moment";

class Order {
	constructor(id, items, totalAmount, orderDate) {
		this.id = id;
		this.items = items;
		this.totalAmount = totalAmount;
		this.orderDate = orderDate;
	}

	get readableOrderDate() {
		// return this.orderDate.toLocaleDateString("en-EN", {
		// 	year: "numeric",
		// 	month: "long",
		// 	day: "numeric",
		// 	hour: "2-digit",
		// 	minute: "2-digit",
		// });
		return moment(this.orderDate).format("MMMM Do YYYY, hh:mm")
	}
}

export default Order;
