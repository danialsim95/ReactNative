class Product {
	constructor(id, ownerId, ownerPushToken, title, imageUrl, description, unitPrice) {
		this.id = id;
		this.ownerId = ownerId;
		this.pushToken = ownerPushToken,
		this.title = title;
		this.imageUrl = imageUrl;
		this.description = description;
		this.unitPrice = unitPrice;
	}
}

export default Product;
