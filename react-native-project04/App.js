import React from "react";
import { LogBox } from "react-native";
import { createStore, combineReducers, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import ReduxThunk from "redux-thunk";
import AppLoading from "expo-app-loading";
import * as Font from "expo-font";
import * as Notifications from "expo-notifications";

import AppNavigator from "./navigations/AppNavigator";
import productReducer from "./store/reducers/product-reducers";
import cartReducer from "./store/reducers/cart-reducers";
import orderReducer from "./store/reducers/order-reducer";
import authReducer from "./store/reducers/auth-reducers";

/* This line removes the "Setting a timer for a long period of time" warning
BUT REMEMBER! It DOES NOT solve the problem.
It is FIREBASE's job to solve this issue. */
LogBox.ignoreLogs(["Setting a timer for a long period of time"]);

Notifications.setNotificationHandler({
	handleNotification: async () => {
		return { shouldShowAlert: true };
	},
});

const rootReducer = combineReducers({
	products: productReducer,
	cart: cartReducer,
	order: orderReducer,
	auth: authReducer,
});

const store = createStore(rootReducer, applyMiddleware(ReduxThunk));

export default function App() {
	const [isFontLoaded] = Font.useFonts({
		"open-sans": require("./assets/fonts/OpenSans-Regular.ttf"),
		"open-sans-bold": require("./assets/fonts/OpenSans-Bold.ttf"),
	});

	if (!isFontLoaded) {
		return <AppLoading />;
	}

	return (
		<Provider store={store}>
			<AppNavigator />
		</Provider>
	);
}
