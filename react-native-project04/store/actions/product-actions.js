import Product from "../../models/product";
import * as Notifications from "expo-notifications";
import * as Permissions from "expo-permissions";

export const FETCH_PRODUCT = "FETCH_PRODUCT";
export const CREATE_PRODUCT = "CREATE_PRODUCT";
export const UPDATE_PRODUCT = "UPDATE_PRODUCT";
export const DELETE_PRODUCT = "DELETE_PRODUCT";

export const fetchProduct = () => {
	return async (dispatch, getState) => {
		/* Execute any async code here */
		const userId = getState().auth.userId;
		try {
			const response = await fetch(
				"https://flutter-dart-12345-default-rtdb.asia-southeast1.firebasedatabase.app/products.json"
			);
			if (!response.ok) {
				throw new Error("Something went wrong.");
			}
			const responseData = await response.json();
			let loadedProducts = [];
			for (const key in responseData) {
				loadedProducts.push(
					new Product(
						key,
						responseData[key].ownerId,
						responseData[key].ownerPushToken,
						responseData[key].productTitle,
						responseData[key].productImageUrl,
						responseData[key].productDescription,
						responseData[key].productUnitPrice
					)
				);
			}
			dispatch({
				type: FETCH_PRODUCT,
				products: loadedProducts,
				userProducts: loadedProducts.filter((prod) => prod.ownerId === userId),
			});
		} catch (error) {
			/* Maybe can send to custom analytic server or etc */
			throw error;
		}
	};
};

export const deleteProduct = (productId) => {
	return async (dispatch, getState) => {
		const userToken = getState().auth.userToken;
		const response = await fetch(
			`https://flutter-dart-12345-default-rtdb.asia-southeast1.firebasedatabase.app/products/${productId}.json?auth=${userToken}`,
			{
				method: "DELETE",
			}
		);
		if (!response.ok) {
			throw new Error("Something went wrong");
		}
		dispatch({
			type: DELETE_PRODUCT,
			productId: productId,
		});
	};
};

export const createProduct = (
	productTitle,
	productDescription,
	productImageUrl,
	productUnitPrice
) => {
	return async (dispatch, getState) => {
		let pushNotiToken;
		/* Check notification permission */
		let notiStatusObj = await Permissions.getAsync(Permissions.NOTIFICATIONS);
		if (notiStatusObj.status !== "granted") {
			/* Ask for notification permission again from the user since not granted yet */
			notiStatusObj = await Permissions.askAsync(Permissions.NOTIFICATIONS);
		}
		if (notiStatusObj.status !== "granted") {
			/* Fine, if the user refuses to grant notification access */
			pushNotiToken = null;
		} else {
			/* Good, the user grants the notification access
			The following step will obtain the push notification token to execute the notification pushing process */
			pushNotiToken = (await Notifications.getExpoPushTokenAsync()).data;
		}
		/* Execute any async code here */
		const userToken = getState().auth.userToken;
		const userId = getState().auth.userId;
		const response = await fetch(
			`https://flutter-dart-12345-default-rtdb.asia-southeast1.firebasedatabase.app/products.json?auth=${userToken}`,
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json",
				},
				body: JSON.stringify({
					productTitle: productTitle,
					productDescription: productDescription,
					productUnitPrice: productUnitPrice,
					productImageUrl: productImageUrl,
					ownerId: userId,
					ownerPushToken: pushNotiToken,
				}),
			}
		);
		const responseData = await response.json();
		dispatch({
			type: CREATE_PRODUCT,
			productData: {
				productId: responseData.name,
				productTitle,
				productDescription,
				productImageUrl,
				productUnitPrice,
				ownerId: userId,
				pushToken: pushNotiToken,
			},
		});
	};
};

export const updateProduct = (productId, productTitle, productDescription, productImageUrl) => {
	return async (dispatch, getState) => {
		const userToken = getState().auth.userToken;
		const response = await fetch(
			`https://flutter-dart-12345-default-rtdb.asia-southeast1.firebasedatabase.app/products/${productId}.json?auth=${userToken}`,
			{
				method: "PATCH",
				headers: {
					"Content-Type": "application/json",
				},
				body: JSON.stringify({
					productTitle: productTitle,
					productDescription: productDescription,
					productImageUrl: productImageUrl,
				}),
			}
		);
		if (!response.ok) {
			throw new Error("Something went wrong");
		}
		dispatch({
			type: UPDATE_PRODUCT,
			productId: productId,
			productData: {
				productTitle,
				productDescription,
				productImageUrl,
			},
		});
	};
};
