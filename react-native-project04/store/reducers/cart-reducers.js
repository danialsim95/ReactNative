import CartItem from "../../models/cart-item";
import { ADD_TO_CART, REMOVE_FROM_CART } from "../actions/cart-actions";
import { ADD_ORDER } from "../actions/order-actions";
import { DELETE_PRODUCT } from "../actions/product-actions";

const initialState = {
	items: {},
	totalAmount: 0,
};

export default (state = initialState, action) => {
	switch (action.type) {
		case ADD_TO_CART:
			const addedProduct = action.product;
			const productTitle = addedProduct.title;
			const productUnitPrice = addedProduct.unitPrice;
			const pushToken = addedProduct.pushToken;
			let updatedOrNewCartItem;
			if (state.items[addedProduct.id]) {
				/* Means the product already exists in the cart, so just add quantity */
				updatedOrNewCartItem = new CartItem(
					state.items[addedProduct.id].quantity + 1,
					productUnitPrice,
					productTitle,
					pushToken,
					state.items[addedProduct.id].subtotal + productUnitPrice
				);
			} else {
				/* Add the product for the first time */
				updatedOrNewCartItem = new CartItem(
					1,
					productUnitPrice,
					productTitle,
					pushToken,
					productUnitPrice
				);
			}
			return {
				...state,
				items: {
					...state.items,
					[addedProduct.id]: updatedOrNewCartItem,
				},
				totalAmount: state.totalAmount + productUnitPrice,
			};
		case REMOVE_FROM_CART:
			const selectedCartItem = state.items[action.productId];
			const currentQty = selectedCartItem.quantity;
			let updatedCartItems;
			if (currentQty > 1) {
				/* Reduce the quantity instead of removing the whole item */
				const updatedCartItem = new CartItem(
					selectedCartItem.quantity - 1,
					selectedCartItem.productUnitPrice,
					selectedCartItem.productTitle,
					selectedCartItem.subtotal - selectedCartItem.productUnitPrice,
				);
				updatedCartItems = { ...state.items, [action.productId]: updatedCartItem }
			} else {
				/* Then really have to remove it */
				updatedCartItems = { ...state.items };
				delete updatedCartItems[action.productId];
			}
			return {
				...state,
				items: updatedCartItems,
				totalAmount: state.totalAmount - selectedCartItem.productUnitPrice,
			};
		case ADD_ORDER:
			return initialState;
		case DELETE_PRODUCT:
			if (!state.items[action.productId]) {
				return state;
			}
			const updatedItems = {...state.items};
			const itemSubTotal = state.items[action.productId].subtotal;
			delete updatedItems[action.productId];
			return {
				...state,
				items: updatedItems,
				totalAmount: state.totalAmount - itemSubTotal,
			}
		default:
			return state;
	}
};
