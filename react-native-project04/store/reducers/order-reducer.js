import { ADD_ORDER, FETCH_ORDER } from "../actions/order-actions";
import Order from "../../models/order";

const initialState = {
	orders: [],
};

export default (state = initialState, action) => {
	switch (action.type) {
		case FETCH_ORDER:
			return {
				orders: action.orders,
			};
		case ADD_ORDER:
			const newOrder = new Order(
				action.orderData.orderId,
				action.orderData.items,
				action.orderData.totalAmount,
				action.orderData.orderDate
			);
			return {
				...state,
				orders: state.orders.concat(newOrder),
			};
	}
	return state;
};
