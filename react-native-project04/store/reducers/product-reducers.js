import Product from "../../models/product";
import {
	CREATE_PRODUCT,
	DELETE_PRODUCT,
	FETCH_PRODUCT,
	UPDATE_PRODUCT,
} from "../actions/product-actions";

const initialState = {
	availableProducts: [],
	userProducts: [],
};

export default (state = initialState, action) => {
	switch (action.type) {
		case FETCH_PRODUCT:
			return {
				availableProducts: action.products,
				userProducts: action.userProducts,
			};
		case CREATE_PRODUCT:
			const newProduct = new Product(
				action.productData.productId,
				action.productData.ownerId,
				action.productData.pushToken,
				action.productData.productTitle,
				action.productData.productImageUrl,
				action.productData.productDescription,
				action.productData.productUnitPrice
			);
			return {
				...state,
				availableProducts: state.availableProducts.concat(newProduct),
				userProducts: state.userProducts.concat(newProduct),
			};
		case UPDATE_PRODUCT:
			const userProductIndex = state.userProducts.findIndex(
				(prod) => prod.id === action.productId
			);
			const availableProductIndex = state.availableProducts.findIndex(
				(prod) => prod.id === action.productId
			);
			const updatedProduct = new Product(
				action.productId,
				state.userProducts[userProductIndex].ownerId,
				state.userProducts[userProductIndex].pushToken,
				action.productData.productTitle,
				action.productData.productImageUrl,
				action.productData.productDescription,
				state.userProducts[userProductIndex].unitPrice
			);
			const updatedUserProducts = [...state.userProducts];
			updatedUserProducts[userProductIndex] = updatedProduct;
			const updatedAvailableProducts = [...state.availableProducts];
			updatedAvailableProducts[availableProductIndex] = updatedProduct;
			return {
				...state,
				availableProducts: updatedAvailableProducts,
				userProducts: updatedUserProducts,
			};
		case DELETE_PRODUCT:
			return {
				...state,
				userProducts: state.userProducts.filter(
					(product) => product.id !== action.productId
				),
				availableProducts: state.availableProducts.filter(
					(product) => product.id !== action.productId
				),
			};
		default:
			return state;
	}
};
