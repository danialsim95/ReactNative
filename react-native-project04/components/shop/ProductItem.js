import React from "react";
import {
	View,
	StyleSheet,
	Text,
	Image,
	TouchableOpacity,
	TouchableNativeFeedback,
	Platform,
} from "react-native";
import Card from "../ui/Card";

const ProductItem = (props) => {
	let TouchableComponent = TouchableOpacity;

	if (Platform.OS === "android" && Platform.Version >= 21) {
		TouchableComponent = TouchableNativeFeedback;
	}

	return (
		<Card style={styles.productItem}>
			<View style={styles.productTouchable}>
				<TouchableComponent onPress={props.onSelect} useForeground>
					<View>
						<View style={styles.productImageContainer}>
							<Image style={styles.productImage} source={{ uri: props.imageUrl }} />
						</View>
						<View style={styles.productDetails}>
							<Text style={styles.productTitle}>{props.title}</Text>
							<Text style={styles.productPrice}>${props.unitPrice.toFixed(2)}</Text>
						</View>
						<View style={styles.actions}>{props.children}</View>
					</View>
				</TouchableComponent>
			</View>
		</Card>
	);
};

const styles = StyleSheet.create({
	productItem: {
		height: 300,
		marginHorizontal: 16,
		marginVertical: 8,
	},
	productTouchable: {
		overflow: "hidden",
		borderRadius: 8,
	},
	productImageContainer: {
		width: "100%",
		height: "60%",
		borderTopLeftRadius: 8,
		borderTopRightRadius: 8,
		overflow: "hidden",
	},
	productDetails: {
		alignItems: "center",
		height: "17%",
		padding: 8,
	},
	productImage: {
		width: "100%",
		height: "100%",
	},
	productTitle: {
		fontSize: 18,
		marginVertical: 4,
		fontFamily: "open-sans-bold",
	},
	productPrice: {
		fontSize: 14,
		color: "#888",
		marginVertical: 4,
		fontFamily: "open-sans",
	},
	actions: {
		height: "23%",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		paddingHorizontal: 16,
	},
});

export default ProductItem;
