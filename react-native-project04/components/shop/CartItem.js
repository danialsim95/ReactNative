import React from "react";
import { View, StyleSheet, Text, TouchableOpacity, Platform } from "react-native";
import { Ionicons } from "@expo/vector-icons";

const CartItem = (props) => {
	return (
		<View style={styles.cartItem}>
			<View style={styles.cartItemData}>
				<Text style={styles.cartItemQuantity}>{props.cartItemQuantity} </Text>
				<Text style={styles.cartItemText}>{props.cartItemTitle}</Text>
			</View>
			<View style={styles.cartItemData}>
				<Text style={styles.cartItemText}>${props.cartItemSubtotal.toFixed(2)}</Text>
				{props.deletable && <TouchableOpacity onPress={props.onRemoveCartItem} style={styles.removeCartItem}>
					<Ionicons
						name={Platform.OS === "android" ? "md-trash" : "ios-trash"}
						size={24}
						color="red"
					/>
				</TouchableOpacity>}
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	cartItem: {
		padding: 12,
		backgroundColor: "white",
		flexDirection: "row",
		justifyContent: "space-between",
		marginHorizontal: 4,
	},
	cartItemData: {
		flexDirection: "row",
		alignItems: "center",
	},
	cartItemQuantity: {
		fontFamily: "open-sans",
		color: "#888",
		fontSize: 16,
	},
	cartItemText: {
		fontFamily: "open-sans-bold",
		fontSize: 16,
	},
	removeCartItem: {
		marginLeft: 8,
	},
});

export default CartItem;
