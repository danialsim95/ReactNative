import React, { useState, useReducer, useCallback, useEffect } from "react";
import {
	ScrollView,
	View,
	Button,
	KeyboardAvoidingView,
	ActivityIndicator,
	Alert,
	StyleSheet,
} from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { useDispatch } from "react-redux";

import Input from "../../components/ui/Input";
import Card from "../../components/ui/Card";
import Colors from "../../constants/Colors";
import * as authActions from "../../store/actions/auth-actions";

const FORM_INPUT_UPDATE = "FORM_INPUT_UPDATE";

const formReducer = (state, action) => {
	if (action.type === FORM_INPUT_UPDATE) {
		const updatedValues = {
			...state.inputValues,
			[action.inputIdentifier]: action.value,
		};
		const updatedValidities = {
			...state.inputValidities,
			[action.inputIdentifier]: action.inputValidity,
		};
		let isFormValidUpdate = true;
		for (const key in updatedValidities) {
			isFormValidUpdate = isFormValidUpdate && updatedValidities[key];
		}
		return {
			inputValues: updatedValues,
			inputValidities: updatedValidities,
			isFormValid: isFormValidUpdate,
		};
	}
	return state;
};

const AuthScreen = (props) => {
	const [isSignupMode, setIsSignupMode] = useState(false);
	const [isLoading, setIsLoading] = useState(false);
	const [error, setError] = useState(null);
	const dispatch = useDispatch();

	const [formState, dispatchFormState] = useReducer(formReducer, {
		inputValues: {
			userEmail: "",
			userPassword: "",
		},
		inputValidities: {
			userEmail: false,
			userPassword: false,
		},
		isFormValid: false,
	});

	const inputChangedHandler = useCallback(
		(inputIdentifier, inputValue, isInputValid) => {
			dispatchFormState({
				type: FORM_INPUT_UPDATE,
				value: inputValue,
				inputValidity: isInputValid,
				inputIdentifier: inputIdentifier,
			});
		},
		[dispatchFormState]
	);

	useEffect(() => {
		if (error) {
			Alert.alert("Error", error, [{ text: "OK" }]);
		}
	});

	const authenticateUserHandler = async () => {
		let action;
		if (isSignupMode) {
			action = authActions.signupUser(
				formState.inputValues.userEmail,
				formState.inputValues.userPassword
			);
		} else {
			action = authActions.signinUser(
				formState.inputValues.userEmail,
				formState.inputValues.userPassword
			);
		}
		setError(null);
		setIsLoading(true);
		try {
			await dispatch(action);
		} catch (error) {
			setError(error.message);
			setIsLoading(false);
		}
	};

	return (
		<KeyboardAvoidingView
			style={styles.screen}
			behavior={Platform.OS === "ios" ? "padding" : null}
			keyboardVerticalOffset={Platform.select({ ios: 0, android: 500 })}
		>
			<LinearGradient colors={["#ffedff", "#ffe3ff"]} style={styles.bgGradient}>
				<Card style={styles.authContainer}>
					<ScrollView>
						<Input
							id="userEmail"
							labelName="Email"
							keyboardType="email-address"
							required
							email
							autoCapitalize="none"
							errorMessage="Please enter a valid email address."
							onInputChanged={inputChangedHandler}
							initialValue=""
						/>
						<Input
							id="userPassword"
							labelName="Password"
							keyboardType="default"
							secureTextEntry
							required
							minLength={6}
							autoCapitalize="none"
							errorMessage="Please enter a valid password."
							onInputChanged={inputChangedHandler}
							initialValue=""
						/>
						<View style={styles.buttonContainer}>
							{isLoading ? (
								<ActivityIndicator size="small" color={Colors.primaryColor} />
							) : (
								<Button
									title={isSignupMode ? "Signup" : "Login"}
									color={Colors.primaryColor}
									onPress={authenticateUserHandler}
								/>
							)}
						</View>
						<View style={styles.buttonContainer}>
							<Button
								title={`Switch to ${isSignupMode ? "Login" : "Signup"}`}
								color={Colors.accentColor}
								onPress={() => {
									setIsSignupMode((prevState) => !prevState);
								}}
							/>
						</View>
					</ScrollView>
				</Card>
			</LinearGradient>
		</KeyboardAvoidingView>
	);
};

export const screenOptions = {
	headerTitle: "Authentication",
};

const styles = StyleSheet.create({
	screen: {
		flex: 1,
	},
	bgGradient: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
	},
	authContainer: {
		width: "80%",
		maxWidth: 400,
		maxHeight: 400,
		padding: 24,
	},
	buttonContainer: {
		marginTop: 8,
	},
});

export default AuthScreen;
