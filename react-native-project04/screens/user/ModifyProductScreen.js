import React, { useState, useEffect, useCallback, useReducer } from "react";
import { View, ScrollView, Alert, StyleSheet, Platform, KeyboardAvoidingView } from "react-native";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import { useSelector, useDispatch } from "react-redux";

import HeaderButton from "../../components/ui/HeaderButton";
import LoadingSpinner from "../../components/ui/LoadingSpinner";
import Input from "../../components/ui/Input";
import * as productActions from "../../store/actions/product-actions";

const FORM_INPUT_UPDATE = "FORM_INPUT_UPDATE";

const formReducer = (state, action) => {
	if (action.type === FORM_INPUT_UPDATE) {
		const updatedValues = {
			...state.inputValues,
			[action.inputIdentifier]: action.value,
		};
		const updatedValidities = {
			...state.inputValidities,
			[action.inputIdentifier]: action.inputValidity,
		};
		let isFormValidUpdate = true;
		for (const key in updatedValidities) {
			isFormValidUpdate = isFormValidUpdate && updatedValidities[key];
		}
		return {
			inputValues: updatedValues,
			inputValidities: updatedValidities,
			isFormValid: isFormValidUpdate,
		};
	}
	return state;
};

const ModifyProductScreen = (props) => {
	const [isLoading, setIsLoading] = useState(false);
	const [error, setError] = useState(null);

	const productId = props.route.params ? props.route.params.productId : null;
	const editedProduct = useSelector((state) =>
		state.products.userProducts.find((prod) => prod.id === productId)
	);
	const dispatch = useDispatch();

	/* Price shouldn't be changeable, therefore it will be true if there is editedProduct */
	const [formState, dispatchFormState] = useReducer(formReducer, {
		inputValues: {
			productTitle: editedProduct ? editedProduct.title : "",
			productImageUrl: editedProduct ? editedProduct.imageUrl : "",
			productUnitPrice: "",
			productDescription: editedProduct ? editedProduct.description : "",
		},
		inputValidities: {
			productTitle: editedProduct ? true : false,
			productImageUrl: editedProduct ? true : false,
			productUnitPrice: editedProduct ? true : false,
			productDescription: editedProduct ? true : false,
		},
		isFormValid: editedProduct ? true : false,
	});

	const submitFormHandler = useCallback(async () => {
		if (!formState.isFormValid) {
			/* Cancel the function execution if the form is invalid! */
			Alert.alert("Error", "There is something wrong on the form. Please check.", [
				{
					text: "OK",
				},
			]);
			return;
		}
		setIsLoading(true);
		setError(null);
		try {
			if (editedProduct) {
				await dispatch(
					productActions.updateProduct(
						productId,
						formState.inputValues.productTitle,
						formState.inputValues.productDescription,
						formState.inputValues.productImageUrl
					)
				);
			} else {
				await dispatch(
					productActions.createProduct(
						formState.inputValues.productTitle,
						formState.inputValues.productDescription,
						formState.inputValues.productImageUrl,
						+formState.inputValues.productUnitPrice
					)
				);
			}
			props.navigation.goBack();
		} catch (error) {
			setError(error.message);
		}
		setIsLoading(false);
	}, [dispatch, productId, formState]);

	useEffect(() => {
		props.navigation.setOptions({
			headerRight: () => (
				<HeaderButtons HeaderButtonComponent={HeaderButton}>
					<Item
						title="Save"
						iconName={Platform.OS === "android" ? "md-checkmark" : "ios-checkmark"}
						onPress={submitFormHandler}
					/>
				</HeaderButtons>
			),
		});
	}, [submitFormHandler]);

	useEffect(() => {
		if (error) {
			Alert.alert("Error", error, [
				{
					text: "OK",
				},
			]);
		}
	}, [error]);

	const inputChangedHandler = useCallback(
		(inputIdentifier, inputValue, isInputValid) => {
			dispatchFormState({
				type: FORM_INPUT_UPDATE,
				value: inputValue,
				inputValidity: isInputValid,
				inputIdentifier: inputIdentifier,
			});
		},
		[dispatchFormState]
	);

	if (isLoading) {
		return <LoadingSpinner />;
	}

	return (
		<KeyboardAvoidingView
			style={{ flex: 1 }}
			behavior={Platform.OS === "ios" ? "padding" : null}
			keyboardVerticalOffset={Platform.select({ ios: 0, android: 500 })}
		>
			<ScrollView>
				<View style={styles.form}>
					<Input
						id="productTitle"
						labelName="Title"
						errorMessage="Please enter a valid title!"
						keyboardType="default"
						autoCapitalize="sentences"
						autoCorrect
						returnKeyType="next"
						onInputChanged={inputChangedHandler}
						initialValue={editedProduct ? editedProduct.title : ""}
						initialValidity={!!editedProduct}
						required
					/>
					<Input
						id="productImageUrl"
						labelName="Image URL"
						errorMessage="Please enter a valid image url!"
						onInputChanged={inputChangedHandler}
						initialValue={editedProduct ? editedProduct.imageUrl : ""}
						initialValidity={!!editedProduct}
						required
					/>
					{editedProduct ? null : (
						<Input
							id="productUnitPrice"
							labelName="Unit Price ($)"
							errorMessage="Please enter a valid unit price!"
							keyboardType="decimal-pad"
							onInputChanged={inputChangedHandler}
							required
							min={0.1}
						/>
					)}
					<Input
						id="productDescription"
						labelName="Description"
						errorMessage="Please enter a valid description!"
						keyboardType="default"
						autoCapitalize="sentences"
						autoCorrect
						multiline
						numberOfLines={3}
						onInputChanged={inputChangedHandler}
						initialValue={editedProduct ? editedProduct.description : ""}
						initialValidity={!!editedProduct}
						required
						minLength={6}
					/>
				</View>
			</ScrollView>
		</KeyboardAvoidingView>
	);
};

const styles = StyleSheet.create({
	form: {
		margin: 24,
	},
	centered: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
	},
});

export const screenOptions = (navigationData) => {
	const routeParams = navigationData.route.params ? navigationData.route.params : {};
	return {
		headerTitle: routeParams.productId ? "Edit Product" : "Add Product",
	};
};

export default ModifyProductScreen;
