import React, { useEffect } from "react";
import { View, ActivityIndicator, StyleSheet } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useDispatch } from "react-redux";

import Colors from "../constants/Colors";
import * as authActions from "../store/actions/auth-actions";

const SplashScreen = (props) => {
    const dispatch = useDispatch();

    useEffect(() => {
        const tryLogin = async () => {
            const userData = await AsyncStorage.getItem("userData");
            if (!userData) {
                /* We are certainly not logged in */
                dispatch(authActions.setDidTryAutoLogin());
                return;
            }
            const transformedUserData = JSON.parse(userData);
            const { userToken, userId, expiryDate } = transformedUserData;
            const expirationDate = new Date(expiryDate);
            /* Invalid */
            if (expirationDate <= new Date() || !userToken || !userId) {
                dispatch(authActions.setDidTryAutoLogin());
                return;
            }
            /* Past if check */
            const remainingExpirationTime = expirationDate.getTime() - new Date().getTime();
            dispatch(authActions.authenticateUser(userId, userToken, remainingExpirationTime));
        };
        tryLogin();
    }, [dispatch]);

	return (
		<View style={styles.screen}>
			<ActivityIndicator size="large" color={Colors.primaryColor} />
		</View>
	);
};

const styles = StyleSheet.create({
	screen: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
	},
});

export default SplashScreen;
