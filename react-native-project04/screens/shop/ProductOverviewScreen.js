import React, { useState, useEffect, useCallback } from "react";
import { FlatList, View, Text, Button, Platform } from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { HeaderButtons, Item } from "react-navigation-header-buttons";

import ProductItem from "../../components/shop/ProductItem";
import HeaderButton from "../../components/ui/HeaderButton";
import LoadingSpinner from "../../components/ui/LoadingSpinner";
import Fallback from "../../components/ui/Fallback";
import * as cartActions from "../../store/actions/cart-actions";
import * as productActions from "../../store/actions/product-actions";
import Colors from "../../constants/Colors";

const ProductOverviewScreen = (props) => {
	const [isLoading, setIsLoading] = useState(false);
	const [isRefreshing, setIsRefreshing] = useState(false);
	const [error, setError] = useState(null);
	const productList = useSelector((state) => state.products.availableProducts);
	const dispatch = useDispatch();

	const loadProducts = useCallback(async () => {
		setError(null);
		setIsRefreshing(true);
		try {
			await dispatch(productActions.fetchProduct());
		} catch (error) {
			setError(error.message);
		}
		setIsRefreshing(false);
	}, [dispatch, setIsLoading, setError]);

	/* We need this extra useEffect call for initial loading purposes */
	useEffect(() => {
		/* Initial load */
		setIsLoading(true);
		loadProducts().then(() => {
			setIsLoading(false);
		});
	}, [dispatch, loadProducts]);

	useEffect(() => {
		const unsubscribeFocusSub = props.navigation.addListener("focus", loadProducts);
		return () => {
			unsubscribeFocusSub();
		};
	}, [loadProducts]);

	const selectItemHandler = (id, title) => {
		props.navigation.navigate("ProductDetail", {
			productId: id,
			productTitle: title,
		});
	};

	if (isLoading) {
		return <LoadingSpinner />;
	}

	if (error) {
		return (
			<Fallback>
				<Text>An error occurred. Please try again.</Text>
				<View style={{ marginTop: 8 }}>
					<Button title="Try again" onPress={loadOrders} color={Colors.primaryColor} />
				</View>
			</Fallback>
		);
	}

	if (!isLoading && productList.length === 0) {
		return (
			<Fallback>
				<Text>No products found. Let's start adding some!</Text>
			</Fallback>
		);
	}

	return (
		<FlatList
			onRefresh={loadProducts}
			refreshing={isRefreshing}
			data={productList}
			keyExtractor={(productItem) => productItem.id}
			renderItem={(productItem) => (
				<ProductItem
					title={productItem.item.title}
					unitPrice={productItem.item.unitPrice}
					imageUrl={productItem.item.imageUrl}
					onSelect={() => {
						selectItemHandler(productItem.item.id, productItem.item.title);
					}}
				>
					<Button
						color={Colors.primaryColor}
						title="View Details"
						onPress={() => {
							selectItemHandler(productItem.item.id, productItem.item.title);
						}}
					/>
					<Button
						color={Colors.primaryColor}
						title="Add To Cart"
						onPress={() => {
							dispatch(cartActions.addToCart(productItem.item));
						}}
					/>
				</ProductItem>
			)}
		/>
	);
};

export const screenOptions = (navigationData) => {
	return {
		headerTitle: "All Products",
		headerLeft: () => (
			<HeaderButtons HeaderButtonComponent={HeaderButton}>
				<Item
					title="Menu"
					iconName={Platform.OS === "android" ? "md-menu" : "ios-menu"}
					onPress={() => {
						navigationData.navigation.toggleDrawer();
					}}
				/>
			</HeaderButtons>
		),
		headerRight: () => (
			<HeaderButtons HeaderButtonComponent={HeaderButton}>
				<Item
					title="Cart"
					iconName={Platform.OS === "android" ? "md-cart" : "ios-cart"}
					onPress={() => {
						navigationData.navigation.navigate("Cart");
					}}
				/>
			</HeaderButtons>
		),
	};
};

export default ProductOverviewScreen;
