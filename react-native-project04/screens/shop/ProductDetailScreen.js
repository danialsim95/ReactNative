import React from "react";
import { ScrollView, View, StyleSheet, Text, Image, Button } from "react-native";
import { useSelector, useDispatch } from "react-redux";

import Colors from "../../constants/Colors";
import * as cartActions from "../../store/actions/cart-actions";

const ProductDetailScreen = (props) => {
	const productId = props.route.params.productId;
	const selectedProduct = useSelector((state) => state.products.availableProducts).find(
		(prod) => prod.id === productId
	);
	const dispatch = useDispatch();

	return (
		<ScrollView>
			<Image style={styles.productImage} source={{ uri: selectedProduct.imageUrl }} />
			<View style={styles.actionContainer}>
				<Button color={Colors.primaryColor} title="Add to Cart" onPress={() => {
					dispatch(cartActions.addToCart(selectedProduct));
				}} />
			</View>
			<Text style={styles.productUnitPrice}>${selectedProduct.unitPrice.toFixed(2)}</Text>
			<Text style={styles.productDescription}>{selectedProduct.description}</Text>
		</ScrollView>
	);
};

const styles = StyleSheet.create({
	productImage: {
		width: "100%",
		height: 300,
	},
	actionContainer: {
		marginVertical: 12,
		alignItems: "center",
	},
	productUnitPrice: {
		fontSize: 20,
		color: "#888",
		textAlign: "center",
		marginVertical: 16,
        fontFamily: "open-sans-bold",
	},
	productDescription: {
		fontSize: 14,
		textAlign: "center",
        marginHorizontal: 20,
        fontFamily: "open-sans",
	},
});

export const screenOptions = (navigationData) => {
	return {
		headerTitle: navigationData.route.params.productTitle,
	};
};

export default ProductDetailScreen;
