import React, { useState } from "react";
import { View, Text, Button, StyleSheet, FlatList, ActivityIndicator } from "react-native";
import { useSelector, useDispatch } from "react-redux";

import CartItem from "../../components/shop/CartItem";
import Card from "../../components/ui/Card";
import Colors from "../../constants/Colors";
import * as cartActions from "../../store/actions/cart-actions";
import * as orderActions from "../../store/actions/order-actions";

const CartScreen = (props) => {
	const [isLoading, setIsLoading] = useState(false);
	const cartTotalAmount = useSelector((state) => state.cart.totalAmount);
	const cartItems = useSelector((state) => {
		const transformedCartItems = [];
		for (const key in state.cart.items) {
			transformedCartItems.push({
				productId: key,
				productTitle: state.cart.items[key].productTitle,
				productUnitPrice: state.cart.items[key].productUnitPrice,
				quantity: state.cart.items[key].quantity,
				subtotal: state.cart.items[key].subtotal,
				productPushToken: state.cart.items[key].pushToken,
			});
		}
		return transformedCartItems.sort((a, b) => (a.productId > b.productId ? 1 : -1));
	});

	const dispatch = useDispatch();

	const sendOrderHandler = async () => {
		setIsLoading(true);
		await dispatch(orderActions.addOrder(cartItems, cartTotalAmount));
		setIsLoading(false);
		props.navigation.navigate("ProductOverview");
	};

	return (
		<View style={styles.screen}>
			<Card style={styles.cartSummary}>
				<Text style={styles.cartSummaryText}>
					Total:{" "}
					<Text style={styles.cartAmount}>
						${(Math.round(cartTotalAmount * 100) / 100).toFixed(2)}
					</Text>
				</Text>
				{isLoading ? (
					<ActivityIndicator size="small" color={Colors.primaryColor} />
				) : (
					<Button
						disabled={cartItems.length === 0}
						color={Colors.accentColor}
						title="Order Now"
						onPress={sendOrderHandler}
					/>
				)}
			</Card>
			<FlatList
				data={cartItems}
				keyExtractor={(item) => item.productId}
				renderItem={(cartItemData) => (
					<CartItem
						cartItemQuantity={cartItemData.item.quantity}
						cartItemTitle={cartItemData.item.productTitle}
						cartItemSubtotal={cartItemData.item.subtotal}
						deletable
						onRemoveCartItem={() => {
							dispatch(cartActions.removeFromCart(cartItemData.item.productId));
						}}
					/>
				)}
			/>
		</View>
	);
};

const styles = StyleSheet.create({
	screen: {
		margin: 20,
	},
	cartSummary: {
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between",
		marginBottom: 20,
		padding: 12,
	},
	cartSummaryText: {
		fontFamily: "open-sans-bold",
		fontSize: 18,
	},
	cartAmount: {
		color: Colors.primaryColor,
	},
});

export const screenOptions = {
	headerTitle: "Your Cart",
};

export default CartScreen;
