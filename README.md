# ReactNative
This folder is all about React Native. All projects were generated with [React Native](https://reactnative.dev/). All projects here have been built under React 16.13.1 with React SDK 42 in August 2021.

## :pushpin: React Native Project 01 (react-native-project01)
Basics of React Native with the Course Goal App

### Content
1. Components
1. Styles (inclusive of `StyleSheet`)
1. Layouts
1. State (`useState()`) & Events
1. Lists (inclusive of `FlatList`)
1. ScrollView
1. Modal

## :pushpin: React Native Project 02 (react-native-project02)
Basics Deep Dive + Adaptive User Interfaces with the Guess A Number App

### Content
1. Custom Component
1. Advanced Styling and Theming (with more flexible rules)
1. TextInput (using Custom Component)
1. Alert
1. Multiple Screens handling
1. React Hooks (`useEffect()`)
1. Custom Fonts
1. Images (inclusive of network images)
1. Icons
1. `Dimensions` API
1. `ScreenOrientation` API from Expo
1. `Platform` API
1. `SafeAreaView` Component

## :pushpin: React Native Project 03 (react-native-project03)
React Navigation + State Management & Redux with the Meals App

### Content
1. React Navigation (`StackNavigator`, `BottomTabNavigator`, `DrawerNavigator`)
1. Navigation Configuration
1. `HeaderButtons`
1. State Management
1. Redux
1. More hooks (`useCallback()`, `useReducer()`)

## :pushpin: React Native Project 04 (react-native-project04)
React Native Core + Handling User Input + HTTP Requests + User Authentication with the Shop App

### Content
1. Cores of React Native (Recap)
1. Handling User Input (Validation, Form and Input Management)
1. Soft Keyboard Handling
1. HTTP Requests (Redux Thunk)
1. Loading Spinner (`ActivityIndicator`)
1. Pull to Refresh (`onRefresh` and `refreshing` attributes in `FlatList`)
1. User Authentication
1. Store data locally with `AsyncStorage`
1. React Navigation v5 update

**UPDATE AUGUST 2021: This project is also used to migrate with React Navigation v5.

## :pushpin: React Native Project 05 (react-native-project05)
Native Device Features + App Publishing with the Great Places App

### Content
1. Device Camera
1. SQLite
1. Getting User Location
1. File Systems
1. Maps
1. Configure App Icons
1. Configure Splash Screen