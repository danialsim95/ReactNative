import React, { useCallback, useEffect, useState } from "react";
import { Text, StyleSheet, TouchableOpacity, Platform } from "react-native";
import MapView, { Marker } from "react-native-maps";

import Colors from "../constants/Colors";

const MapScreen = (props) => {
	const initialLocation = props.navigation.getParam("initialLocation");
	const readonly = props.navigation.getParam("readonly");
	const [selectedLocation, setSelectedLocation] = useState(initialLocation);

	const mapRegion = {
		latitude: initialLocation ? initialLocation.latitude : 2.967,
		longitude: initialLocation ? initialLocation.longitude : 101.803,
		latitudeDelta: 0.1,
		longitudeDelta: 0.05,
	};

	const selectLocationHandler = (event) => {
		if (readonly) {
			/* Disable the function to pick location */
			return;
		}
		setSelectedLocation({
			latitude: event.nativeEvent.coordinate.latitude,
			longitude: event.nativeEvent.coordinate.longitude,
		});
	};

	const savePickedLocationHandler = useCallback(() => {
		if (!selectedLocation) {
			/* May show an alert if needed */
			return;
		}
		props.navigation.navigate("NewPlace", {
			pickedLocation: selectedLocation,
		});
	}, [selectedLocation]);

	useEffect(() => {
		props.navigation.setParams({
			saveLocation: savePickedLocationHandler,
		});
	}, [savePickedLocationHandler]);

	return (
		<MapView style={styles.map} region={mapRegion} onPress={selectLocationHandler}>
			{selectedLocation && (
				<Marker title="Picked Location" coordinate={selectedLocation}></Marker>
			)}
		</MapView>
	);
};

MapScreen.navigationOptions = (navigationData) => {
	const saveFunction = navigationData.navigation.getParam("saveLocation");
	const readonly = navigationData.navigation.getParam("readonly");
	if (!readonly) {
		return {
			headerRight: () => (
				<TouchableOpacity style={styles.headerButtonContainer} onPress={saveFunction}>
					<Text style={styles.headerButtonText}>Save</Text>
				</TouchableOpacity>
			),
		};
	}
};

const styles = StyleSheet.create({
	map: {
		flex: 1,
	},
	headerButtonContainer: {
		marginHorizontal: 20,
	},
	headerButtonText: {
		fontSize: 16,
		color: Platform.OS === "android" ? "white" : Colors.primaryColor,
	},
});

export default MapScreen;
