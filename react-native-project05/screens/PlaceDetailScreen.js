import React from "react";
import { ScrollView, Image, View, Text, StyleSheet } from "react-native";
import { useSelector } from "react-redux";

import MapPreview from "../components/MapPreview";
import Colors from "../constants/Colors";

const PlaceDetailScreen = (props) => {
	const placeId = props.navigation.getParam("placeId");
	const selectedPlace = useSelector((state) =>
		state.place.places.find((place) => place.placeId === placeId)
	);

	const selectedLocation = {
		latitude: selectedPlace.placeLatitude,
		longitude: selectedPlace.placeLongitude,
	};

	const showMapHandler = () => {
		props.navigation.navigate("Map", {
			readonly: true,
			initialLocation: selectedLocation,
		});
	};

	return (
		<ScrollView contentContainerStyle={styles.screen}>
			<Image source={{ uri: selectedPlace.placeImageUri }} style={styles.placeImage} />
			<View style={styles.placeLocationContainer}>
				<View style={styles.placeAddressContainer}>
					<Text style={styles.placeAddressText}>{selectedPlace.placeAddress}</Text>
				</View>
				<MapPreview style={styles.placeMapPreview} location={selectedLocation} />
			</View>
		</ScrollView>
	);
};

PlaceDetailScreen.navigationOptions = (navigationData) => {
	return {
		headerTitle: navigationData.navigation.getParam("placeTitle"),
	};
};

const styles = StyleSheet.create({
	screens: {
		alignItems: "center",
	},
	placeImage: {
		height: "35%",
		minHeight: 300,
		width: "100%",
		backgroundColor: "#ccc",
	},
	placeLocationContainer: {
		marginVertical: 24,
		width: "100%",
		maxWidth: 350,
		justifyContent: "center",
		alignItems: "center",
		shadowColor: "black",
		shadowOpacity: 0.25,
		shadowOffset: { width: 0, height: 2 },
		shadowRadius: 8,
		elevation: 8,
		backgroundColor: "white",
		borderRadius: 8,
	},
	placeAddressContainer: {
		padding: 24,
	},
	placeAddressText: {
		color: Colors.primaryColor,
		textAlign: "center",
	},
	placeMapPreview: {
		width: "100%",
		maxWidth: 350,
		height: 300,
		borderBottomLeftRadius: 8,
		borderBottomRightRadius: 8,
	},
});

export default PlaceDetailScreen;
