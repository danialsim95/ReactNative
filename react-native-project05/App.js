import React from "react";
import { createStore, combineReducers, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import ReduxThunk from "redux-thunk";

import PlacesNavigator from "./navigation/PlacesNavigator";
import placeReducer from "./store/place-reducer";
import { init } from "./helpers/database";

init().then(() => {
	console.log("DB Initialised");
}).catch(error => {
	console.log(`DB failed with error: ${error}`);
});

const rootReducer = combineReducers({
	place: placeReducer,
});

const store = createStore(rootReducer, applyMiddleware(ReduxThunk));

export default function App() {
	return (
		<Provider store={store}>
			<PlacesNavigator />
		</Provider>
	);
}
