import * as SQLite from "expo-sqlite";

const database = SQLite.openDatabase("places.db");

export const init = () => {
	const promiseA = new Promise((resolve, reject) => {
		database.transaction((tx) => {
			tx.executeSql(
				"CREATE TABLE IF NOT EXISTS places (placeId INTEGER PRIMARY KEY NOT NULL, placeTitle TEXT NOT NULL, placeImageUri TEXT NOT NULL, placeAddress TEXT NOT NULL, placeLat REAL NOT NULL, placeLng REAL NOT NULL);",
				[],
				(_) => {
					/* Success function */
                    resolve();
				},
				(_, error) => {
					/* Failed function */
                    reject(error);
				}
			);
		});
	});
    return promiseA;
};

export const insertPlace = (placeTitle, placeImageUri, placeAddress, placeLat, placeLng) => {
    const promiseB = new Promise((resolve, reject) => {
		database.transaction((tx) => {
			tx.executeSql(
				"INSERT INTO places (placeTitle, placeImageUri, placeAddress, placeLat, placeLng) VALUES (?, ?, ?, ?, ?);",
				[placeTitle, placeImageUri, placeAddress, placeLat, placeLng],
				(_, result) => {
					/* Success function */
                    resolve(result);
				},
				(_, error) => {
					/* Failed function */
                    reject(error);
				}
			);
		});
	});
    return promiseB;
};

export const selectPlace = () => {
    const promiseC = new Promise((resolve, reject) => {
		database.transaction((tx) => {
			tx.executeSql(
				"SELECT * FROM places;",
				[],
				(_, result) => {
					/* Success function */
                    resolve(result);
				},
				(_, error) => {
					/* Failed function */
                    reject(error);
				}
			);
		});
	});
    return promiseC;
};
